const express = require("express");
const { Server } = require("socket.io");
const { createServer } = require("http");

const logger = require("./logger");

const app = express();
const server = createServer(app);
const io = new Server(server);

// Emit a socket message on all requests

app.use((req, res, next) => {
  io.emit("request", formatRequest(req));
  next();
});

// Echo back the request to any route and method

["GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"].forEach((method) => {
  app[method.toLowerCase()](`*`, (req, res) => {
    const ip = req.headers["cf-connecting-ip"] || req.ip;
    logger.info({
      ...formatRequest(req),
      message: `${method} ${req.url} FROM ${ip} (${req.headers["user-agent"]})`,
    });
    res.json(formatRequest(req));
  });
});

app.head("*", (req, res) => {
  const ip = req.headers["cf-connecting-ip"] || req.ip;
  logger.info({
    ...formatRequest(req),
    message: "HEAD",
  });
  res.json(formatRequest(req));
});

const port = process.env.PORT || 3000;

logger.info(`Starting server on port ${port}`);

io.on("connection", (socket) => {
  console.log("a user connected");
});

server.listen(port);

function formatRequest(req) {
  return {
    method: req.method,
    url: req.url.split("?")[0],
    headers: req.headers,
    body: req.body,
    query: req.query,
    cookies: req.cookies,
    ip: req.headers["cf-connecting-ip"] || req.ip,
  };
}

