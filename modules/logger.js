const pino = require("pino");
const { LOG_LEVEL, LOGTAIL_TOKEN } = process.env;
const { name, version } = require("../package.json");

const transport = pino.transport({
  targets: [
    ...(LOGTAIL_TOKEN
      ? [{ target: "@logtail/pino", options: { sourceToken: LOGTAIL_TOKEN } }]
      : []),
    { target: "pino-pretty" },
  ],
});

const logger = pino(
  {
    name,
    level: LOG_LEVEL || "info",
    version,
  },
  transport
);

module.exports = logger;
